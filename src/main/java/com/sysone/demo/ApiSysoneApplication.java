package com.sysone.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSysoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSysoneApplication.class, args);
	}

}
