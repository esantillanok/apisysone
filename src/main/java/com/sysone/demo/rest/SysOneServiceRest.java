package com.sysone.demo.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.sysone.demo.model.DataRequest;
import com.sysone.demo.model.DataResponse;
import com.sysone.demo.service.StringService;

@Path("/sysone/")
public class SysOneServiceRest {
	
	
@Autowired
private StringService stringService;
	
	@GET
	@Path("/candidate")	
	public Response getCandidato() {
		return Response.status(200).entity("Eduardo Santillan").build();
	}

	
	@POST	
	@Path("/compress")	
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCertificado(DataRequest request) {
		DataResponse respuesta = new DataResponse();
		String compressed = stringService.compressString(request.getValue().toUpperCase());
		respuesta.setCompressed(compressed);
		return Response.status(200).entity(respuesta).build();

	}	
	
}
